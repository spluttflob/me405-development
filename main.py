""" @file main.py
    Thie file contains a bunch of stuff. Really stuffy stuff. 

    @author JR
    
    @date Tue Dec  8 10:26:15 2020
        
"""

import pyb
import gc
from micropython import const, alloc_emergency_exception_buf
import cotask
import task_share

# Allocate memory so that exceptions raised in interrupt service routines can
# generate useful diagnostic printouts
alloc_emergency_exception_buf (100)


class BusyTask:
    """ Class which contains a task function. This tests the use of a class
    method as a task function, and it allows lots of tasks to be easily made.
    """

    ## A serial number for @c BusyTask objects. This gives each created busy
    #  task a separate identifier. This is an example of a variable which
    #  "belongs to the class" rather than to any one instance of the class; 
    #  note that it's accessed as @c BusyTask.t_num, not @c self.t_num
    t_num = 0

    def __init__ (self):
        """ Initialize a busy task by creating its serial number. """

        ## The serial number of the busy task
        self.ser_num = BusyTask.t_num
        BusyTask.t_num += 1


    def run_fun (self):
        """ Run function for the @c BusyTask.  This function doesn't do much
        except to verify that a class member can be a task function. """

        while True:
            print ('[' + str (self.ser_num) + ']', end='')
            yield 0


## Counter used by the ISR as simulated data
isr_count = 0

## Queue into which the ISR puts things
isr_queue = task_share.Queue ('i', 16, thread_protect=True, overwrite=False, 
                              name="ISR Queue")

def test_ISR (source):
    """ 
    @brief   Timer interrupt handler to test scheduling.
    @details This interrupt service routine is fired by a timer periodically 
             and puts some data into a queue.
    @param   source The number of the timer which caused this ISR to run
    """
    global isr_count
    isr_count += 1
    isr_queue.put (isr_count, in_ISR=True)


def low_pri_fn ():
    """ 
    @brief   Task function which runs at low priority.
    @details This task runs at low priority and takes up a lot of time.
    """
    
    while True:
        yield 0


# ----------------------------------------------------------------------------

if __name__ == "__main__":

    print ("\033[2JTesting scheduler.\r\nPress Ctrl+C to stop test.")

    # Create a bunch of silly time-wasting busy tasks to test how well the
    # scheduler works when it has a lot to do
    for tnum in range (10):
        newb = BusyTask ()
        bname = "Busy_" + str (newb.ser_num)
        cotask.task_list.append (cotask.Task (newb.run_fun, name = bname, 
            priority = tnum + 10, period = 400 + 30 * tnum, profile = True))

    print ("\r\n" + str (cotask.task_list))

    # Run the memory garbage collector to ensure memory is as defragmented as
    # possible before the real-time scheduler is started
    gc.collect ()

    # Set up the timer interrupt handler to take up more processor time
    

    # Run the scheduler with the chosen scheduling algorithm. Quit if a 
    # Ctrl+C character is sent through the serial port
    while True:
        try:
            cotask.task_list.pri_sched ()
        except KeyboardInterrupt:
            print ("\r\nSomeone pressed Ctrl+C.")
            break

    # Print a table of task data and a table of shared information data
    print ("\r\n" + str (cotask.task_list))
    print (task_share.show_all ())
    print ("Memory free: " + str (gc.mem_free ()))
    # print (task1.get_trace ())
    print ("\r\n")








